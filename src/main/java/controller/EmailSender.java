/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.dmssw.orm.controllers.ORMConnection.logger;
import com.sun.mail.smtp.SMTPTransport;
import entities.EmailDetails;
import java.io.FileInputStream;
import static java.lang.ProcessBuilder.Redirect.to;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import jdk.nashorn.internal.runtime.logging.Logger;
import util.ResponseData;

/**
 *
 * @author Sandali Kaushalya
 */
public class EmailSender {

    public static HashMap getMailConfigurations() {

        String mailTransport = null;
        String mailIp = null;
        String smtpsHost = null;
        String mailAddress = null;
        String smtpsAuth = null;
        String sentFrom = null;
        String sentFromPword = null;
        HashMap mailConfig = new HashMap();
        
       

        try {
               
         
            Properties pro = new Properties();
            String wspath = System.getenv("WS_HOME");
            String pathset = wspath.replace("\\", "/");
            String ippath = pathset + "/Majlis_Conn.properties";
            pro.load(new FileInputStream(ippath));

            mailTransport = pro.getProperty("MAIL_TRANSPORT");
            mailConfig.put("mailTransport", mailTransport);
            System.out.println("mailTransport:"+mailTransport);            

            mailIp = pro.getProperty("MAIL_CONNECT_IP");
            mailConfig.put("mailIp", mailIp);
            System.out.println("mailIp:"+mailIp);

            smtpsHost = pro.getProperty("MAIL_SMTPS_HOST");
            mailConfig.put("smtpsHost", smtpsHost);
            System.out.println("smtpsHost:"+smtpsHost);

            mailAddress = pro.getProperty("MAIL_ADDRESS");
            mailConfig.put("mailAddress", mailAddress);
            System.out.println("mailAddress:"+mailAddress);

            smtpsAuth = pro.getProperty("MAIL_SMTPS_AUTH");
            mailConfig.put("smtpsAuth", smtpsAuth);
            System.out.println("smtpsAuth:"+smtpsAuth);

            sentFrom = pro.getProperty("MAIL_SENT_FROM");
            mailConfig.put("sentFrom", sentFrom);
            System.out.println("sentFrom:"+sentFrom);

            sentFromPword = pro.getProperty("MAIL_SENT_FROM_PASSWORD");
            mailConfig.put("sentFromPword", sentFromPword);
            System.out.println("sentFromPword:"+sentFromPword);

        } catch (Exception e) {
            System.out.println("error configurations........."+e);
            e.getMessage();
        }
        return mailConfig;
    }

    @SuppressWarnings("empty-statement")

    public static boolean sendHTMLMail(String toMailAdd,ResponseData data) throws MessagingException {
        boolean isSent = false;
        System.out.println("inside sendHTMLMail............................");
        try {
            System.out.println("inside try sendHTMLMail............................");
            //logger.info("inside try sendHTMLMail............................");
            HashMap mailConfig = EmailSender.getMailConfigurations();
            String mailTransport = mailConfig.get("mailTransport").toString();
            String mailIp = mailConfig.get("mailIp").toString();
            String smtpsHost = mailConfig.get("smtpsHost").toString();
            String mailAddress = mailConfig.get("mailAddress").toString();
            String smtpsAuth = mailConfig.get("smtpsAuth").toString();
            String sentFrom = mailConfig.get("sentFrom").toString();
            String sentFromPword = mailConfig.get("sentFromPword").toString();
            System.out.println(" Email Configs:  " + mailTransport + " --> " + mailIp + " --> " + smtpsHost + " --> " + mailAddress + " --> " + smtpsAuth + " --> " + sentFrom + " -->  " + sentFromPword);
            
            Properties props = System.getProperties();
            props.put(smtpsHost, mailAddress);
            props.put(smtpsAuth, "false");
            Session session = Session.getInstance(props, null);
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(sentFrom));;
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));
            
            EmailDetails det =(EmailDetails)data.getResponseData();
            
            msg.setSubject("Welcome to Majlis!");
            msg.setText("Dear " + " \n\nWelcome to TAPSTAK! \n\n     "
                    +"Group Title:"+det.getGroupTitle()+"\n Decription: "+det.getGroupDescription()+"\n"
                   +"\n\n Event Name:"+det.getEventCaption()+"\n Event Location:"+det.getEventLocation()+"\n Event Message :"+det.getEventMessage()+"\n"
                    + " Mobile User Name         : " +det.getMobileUserName()+ " \n      User Mobile Number : " +det.getMobileNo()+" \n      Mobile User Country : " +det.getMobileUserCounty()+ "\n\n "
                    + "\n\n Marked as:"+det.getStatus());
            msg.setHeader("X-Mailer", "Majlis Event RSVP mail");
            
            msg.setSentDate(new Date());
            SMTPTransport t
                    = (SMTPTransport) session.getTransport(mailTransport);
            t.connect(mailIp, 25, sentFrom, sentFromPword);
            t.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Response: " + t.getLastServerResponse());
            t.close();
            isSent = true;
        } catch (Exception ex) {
            System.out.println("Error in E-mail ........"+ex);
            ex.printStackTrace();
        }

        return isSent;
    }

}

