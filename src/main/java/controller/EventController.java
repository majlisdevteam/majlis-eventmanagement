/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.dmssw.orm.config.AppParams;
import java.sql.Connection;
import util.ResponseData;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisGroupEvent;
import com.dmssw.orm.models.MajlisGroupEventStatus;
import com.dmssw.orm.models.MajlisMobileUsers;
import entities.EmailDetails;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Dilhan Ganegama
 */
public class EventController {

    Logger logger;

    public EventController() {
        logger = AppParams.logger;

    }

    public ResponseData saveRsvpStatus(int eventId, int userId, int statusId) {
        logger.info("saveRsvpStatus method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        try {
            String insertSql = "INSERT INTO majlis_mobile_users_event (EVENT_ID,USER_ID,EVENT_STATUS) "
                    + " VALUES(?,?,?)";

            System.out.println("insertSql" + insertSql);

            PreparedStatement ps = db.prepare(conn, insertSql);

            ps.setInt(1, eventId);
            ps.setInt(2, userId);
            ps.setInt(3, statusId);

            db.executePreparedStatement(conn, ps);
            flag = 1;
            rd.setResponseData("successfull");

        } catch (Exception e) {

            logger.error("Error in saveRsvpStatus method.." + e);
            rd.setResponseData("Error in saveRsvpStatus method..");
            flag = -1;

        }
        rd.setResponseCode(flag);
        return rd;

    }

    public ResponseData getEventStatus(int eventId) {
        logger.info("updateRsvpStatus method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        List<MajlisGroupEventStatus> EventStatusList = new ArrayList<>();

        try {

            String selectQuery = "Select s.STATUS_ID,s.STATUS "
                    + "from majlis_group_event_status s "
                    + "where s.EVENT_ID =" + eventId;

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {

                MajlisGroupEventStatus s = new MajlisGroupEventStatus();

                s.setStatusId(rs.getInt("STATUS_ID"));
                s.setStatus(rs.getString("STATUS"));

                EventStatusList.add(s);
                flag = 1;
                rd.setResponseData("Successfull");

            }
            rd.setResponseData(EventStatusList);

        } catch (Exception e) {

            logger.error("Error in getEventStatus method.." + e);
            flag = -1;
            rd.setResponseData("Error in getEventStatus method..");

        }
        rd.setResponseCode(flag);
        return rd;
    }

    public ResponseData updateRsvpStatus(int eventId, int userId, int statusId) {
        logger.info("updateRsvpStatus method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;
        int count;

        try {
            String selectQuery = "Select count(*) as COUNT  from majlis_mobile_users_event s "
                    + "where s.EVENT_ID='" + eventId + "'"
                    + " AND s.USER_ID='" + userId + "'";

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {

                count = rs.getInt("COUNT");

                if (count > 0) {
                    String deleteQuery = "Delete from majlis_mobile_users_event "
                            + " where EVENT_ID='" + eventId + "'"
                            + " AND USER_ID='" + userId + "'";

                    db.save(conn, deleteQuery);

                }
                rd = saveRsvpStatus(eventId, userId, statusId);

            }
            logger.info("before email............");
            // boolean b = EmailSender.sendHTMLMail("akshitracker@gmail.com");
            ResponseData data = sendEmailToAdmin(eventId, userId, statusId);

            //  logger.info("boolean msg:" + b);
        } catch (Exception e) {
            logger.error("Error in updateRsvpStatus method.." + e);
            rd.setResponseData("Error in updateRsvpStatus method..");
            flag = -1;
            rd.setResponseCode(flag);
            e.printStackTrace();
        }

        return rd;
    }

    public ResponseData sendEmailToAdmin(int eventId, int userId, int statusId) {
        logger.info("updateRsvpStatus method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        try {

            String selectQuery = " select g.GROUP_ADMIN,u.EMAIL_ADDRESS "
                    + " from majlis_group g,majlis_cms_users "
                    + " where g.GROUP_ID = ( select e.GROUP_ID"
                    + " from majlis_group_event "
                    + " where e.EVENT_ID ='" + eventId + "')"
                    + "AND u.USER_ID = g.GROUP_ADMIN";

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {
               // String email = rs.getString("EMAIL_ADDRESS");
                ResponseData data = loadMobileUserDetails(eventId, userId, statusId);
                boolean b = EmailSender.sendHTMLMail("akshitracker@gmail.com", data);
                logger.info("boolean msg:" + b);

            }

        } catch (Exception e) {
            logger.error("Error in sendEmailToAdmin method.." + e);
            rd.setResponseData("Error in sendEmailToAdmin method..");
            flag = -1;
            rd.setResponseCode(flag);
            e.printStackTrace();
        }

        return rd;
    }

    public ResponseData loadMobileUserDetails(int eventId, int userId, int statusId) {
//EmailDetails
        DbCon db = new DbCon();
        Connection conn = db.getCon();

        ResponseData rd = new ResponseData();
        int flag = 0;

        EmailDetails details = new EmailDetails();

        try {
            // get mobile user details
            String getUserDetails = "select u.MOBILE_USER_NAME,u.MOBILE_USER_EMAIL,u.MOBILE_USER_COUNTRY,"
                    + "u.MOBILE_USER_MOBILE_NO "
                    + "from majlis_mobile_users u "
                    + "where u.MOBILE_USER_ID='" + userId + "'";

            ResultSet rs = db.search(conn, getUserDetails);

            while (rs.next()) {
                details.setMobileUserName(rs.getString("MOBILE_USER_NAME"));
                details.setMobilUserEmail(rs.getString("MOBILE_USER_EMAIL"));
                details.setMobileUserCounty(rs.getString("MOBILE_USER_COUNTRY"));
                details.setMobileNo(rs.getString("MOBILE_USER_MOBILE_NO"));

            }

            //get group details
            String getGroupDetails = "select m.GROUP_TITLE,m.GROUP_DESCRIPTION, g.EVENT_CAPTION,g.EVENT_ICON_PATH,"
                    + " g.EVENT_MESSAGE,g.EVENT_LOCATION"
                    + " from majlis_group_event g,majlis_group m"
                    + " where g.EVENT_ID = '" + eventId + "'"
                    + "  AND m.GROUP_ID = g.GROUP_ID";

            ResultSet rs2 = db.search(conn, getGroupDetails);

            while (rs2.next()) {
                details.setGroupTitle(rs2.getString("GROUP_TITLE"));
                details.setGroupDescription(rs2.getString("GROUP_DESCRIPTION"));
                details.setEventCaption(rs2.getString("EVENT_CAPTION"));
                details.setEventIconPath(rs2.getString("EVENT_ICON_PATH"));
                details.setEventMessage(rs2.getString("EVENT_MESSAGE"));
                details.setEventLocation(rs2.getString("EVENT_LOCATION"));

            }

            String getStatus = "select s.STATUS"
                    + " from majlis_group_event_status s"
                    + " where s.STATUS_ID = '" + statusId + "'";

            ResultSet rs3 = db.search(conn, getStatus);

            while (rs3.next()) {
                details.setStatus(rs3.getString("STATUS"));

            }
            flag = 1;
            rd.setResponseCode(flag);
            rd.setResponseData(details);

        } catch (Exception e) {
            logger.error("Error loadUserDetails..." + e);
            flag = -1;
            rd.setResponseCode(flag);
            rd.setResponseData("Error loadUserDetails...");

        }
        return rd;
        //return details;
    }
}
