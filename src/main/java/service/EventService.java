/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisMobileUsers;
import controller.EventController;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hibernate.HibernateException;

/**
 *
 * @author Dilhan Ganegama
 */
@Path("/event")
public class EventService {

    @Path("/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MajlisMobileUsers> listevent() {

        System.out.println("MMMMMMMMMMMMMMMMMMM");

        List<MajlisMobileUsers> li = new ArrayList<>();
        try {
            ORMConnection orm = new ORMConnection();

            List<Object> obj = orm.hqlGetResults("SELECT C FROM MajlisMobileUsers C");

            for (Object object : obj) {

                MajlisMobileUsers m = (MajlisMobileUsers) object;

                li.add(m);
            }

            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;
    }

    @POST
    @Path("/saveRsvpStatus")
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveRsvpStatus(@QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("userId") @DefaultValue("0") int userId,
            @QueryParam("statusId") @DefaultValue("0") int statusId) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.saveRsvpStatus(eventId, userId, statusId)).build();
    }

    @POST
    @Path("/updateRsvpStatus")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateRsvpStatus(@QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("userId") @DefaultValue("0") int userId,
            @QueryParam("statusId") @DefaultValue("0") int statusId) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.updateRsvpStatus(eventId, userId, statusId)).build();
    }

    @GET
    @Path("/getEventStatus")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventStatus(@QueryParam("eventId") @DefaultValue("0") int eventId) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.getEventStatus(eventId)).build();
    }
    @GET
    @Path("/loadMobileUserDetails")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loadMobileUserDetails(@QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("userId") @DefaultValue("0") int userId,
            @QueryParam("statusId") @DefaultValue("0") int statusId) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.loadMobileUserDetails(eventId, userId,statusId)).build();
    }


}
